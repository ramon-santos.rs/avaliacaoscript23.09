#!/bin/bash
clear

echo "-----------------------------------------------"
echo "|           Sobre redirecionadores!	      |"
echo "-----------------------------------------------"
echo
echo "==================================================================="
echo "--- Redirecionador > ---"
echo "> - Serve para direcionar a saída de um comando para outra sáida criando o conteúdo."
echo "Exemplo - ls > resultado.txt"
echo "==================================================================="
echo
echo "==================================================================="
echo "--- Redirecionador >> ---"
echo ">> - Serve para adicionar uma sáida a um saída/arquivo já existente."
echo "Exemplo - pwd >> resultado.txt"
echo "==================================================================="
echo
echo "==================================================================="
echo "--- Redirecionador < ---"
echo "< - Este serve para direcionar um entrada de um arquivo."
echo "Exemplo - cat < resultado.txt"
echo "==================================================================="
echo
echo "==================================================================="
echo "--- Redirecionador | ---"
echo "| - Este redireciona a saída de um comando para a entrada de outro comando"
echo "Exemplo - cat resultado.txt | wc -l"
echo "==================================================================="
echo
echo "FIM"
